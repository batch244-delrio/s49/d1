console.log(`Sawakas Friday na..!`)

// Get POST data

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data) => showPosts(data));

// Show post
const showPosts = (posts) => {
	let postEntries = "";
	posts.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>`
	})

	document.querySelector(`#div-post-entries`).innerHTML = postEntries;
}

// Add Post
// Select the add form using queryselector 
// We listen for the submit button for the event

document.querySelector(`#form-add-post`).addEventListener(`submit`, (e) => {
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1

		}),
		headers: {"Content-type" : "application/json"}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert(`Successfully Added!`);

		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null; 
	});
});

// Update post
const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
    document.querySelector("#txt-edit-body").removeAttribute("disabled")
}

	document.querySelector(`#form-edit-post`).addEventListener(`submit`, (e) => {
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PUT",
		body : JSON.stringify({
			id : document.querySelector("#txt-edit-id").value,
			title : document.querySelector("#txt-edit-title").value,
			body : document.querySelector("#txt-edit-body").value,
			id : 1
		}),
		headers: {"Content-type" : "application/json"}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert(`Successfully Added!`);

		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#txt-edit-title").value = null; 
		document.querySelector("#txt-edit-body").value = null; 
		document.querySelector("#btn-submit-update").setAttribute("disabled", true) 
	})
})


// ACTIVITY
const deletePost = (id) => {
    fetch("https://jsonplaceholder.typicode.com/posts/1")
    document.querySelector(`#post-${id}`).remove();
}